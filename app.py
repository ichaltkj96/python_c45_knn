from flask import Flask, render_template, flash, redirect, url_for, session, request, logging
#from data import Articles
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from functools import wraps

import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn import datasets
from sklearn import metrics
import sys

import numpy.matlib 
import numpy as np 

import os
APP_ROOT = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__)

# Config MySQL
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'kelulusan'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
# init MYSQL
mysql = MySQL(app)

#Articles = Articles()

# Index
@app.route('/')
def index():
    return render_template('home.html')


# Train
@app.route('/train')
def train():
    # Create cursor
    cur = mysql.connection.cursor()
    # Get test
    result = cur.execute("SELECT * FROM train")
    trains = cur.fetchall()
    if result > 0:
        return render_template('train.html', trains=trains)
    else:
        msg = 'No Train Found'
        return render_template('train.html', msg=msg)
    # Close connection
    cur.close()


# Test
@app.route('/test')
def test():
    # Create cursor
    cur = mysql.connection.cursor()
    # Get test
    result = cur.execute("SELECT * FROM test")
    tests = cur.fetchall()
    if result > 0:
        return render_template('test.html', tests=tests)
    else:
        msg = 'No Test Found'
        return render_template('test.html', msg=msg)
    # Close connection
    cur.close()


# Data Pivot
@app.route('/pivot')
def pivot():
    # Create cursor
    cur = mysql.connection.cursor()
    # Get test
    result = cur.execute("SELECT npm, nm_mhs, angkatan, nm_matkul, nilai, semester, status FROM test UNION SELECT npm, nm_mhs, angkatan, nm_matkul, nilai, semester, status FROM train")
    hasil_query_select = cur.fetchall()
    data = pd.DataFrame(hasil_query_select, columns=['npm', 'nm_mhs', 'angkatan', 'nm_matkul', 'nilai', 'semester', 'status'])

    data_pivots = pd.pivot_table(data, values='nilai', index='npm', columns='nm_matkul')
    data_pivots = data_pivots.merge(data[['npm','status']].drop_duplicates(),left_on='npm',right_on='npm')
    data_pivots = data_pivots.fillna(-1) # mengganti nilai nan menjadi -1

    if result > 0:
        return render_template('pivot.html',  tables=[data_pivots.to_html(classes='data')], titles=data_pivots.columns.values)

    else:
        msg = 'No Pivot Found'
        return render_template('pivot.html', msg=msg)
    # Close connection
    cur.close()


# Data Matriks C4.5
@app.route('/matriks_c45')
def matriks_c45():
    # Create cursor
    cur = mysql.connection.cursor()
    # Get test
    result = cur.execute("SELECT npm, nm_mhs, angkatan, nm_matkul, nilai, semester, status FROM test UNION SELECT npm, nm_mhs, angkatan, nm_matkul, nilai, semester, status FROM train")
    hasil_query_select = cur.fetchall()
    data = pd.DataFrame(hasil_query_select, columns=['npm', 'nm_mhs', 'angkatan', 'nm_matkul', 'nilai', 'semester', 'status'])

    data_pivot = pd.pivot_table(data, values='nilai', index='npm', columns='nm_matkul')
    data_pivot = data_pivot.merge(data[['npm','status']].drop_duplicates(),left_on='npm',right_on='npm')
    data_pivot = data_pivot.fillna(-1) # mengganti nilai nan menjadi -1

    X = data_pivot.drop(['npm','status'],axis=1)
    y = data_pivot['status']
    y.loc[y=='TERLAMBAT'] = 'Terlambat'
    y.loc[y=='Tidak Lulus'] = 'Terlambat'
    y.loc[y=='LULUS'] = 'Lulus'
    
    X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state=42)

    clf = DecisionTreeClassifier()
    clf.fit(X_train, y_train)
    print(classification_report(y_test,clf.predict(X_test)))
    reports = metrics.classification_report(y_test,clf.predict(X_test), target_names=['Lulus', 'Terlambat'])
    confusion_matrix = metrics.classification_report(y_test,clf.predict(X_test))
    
    f = open('static/classification_report.txt', 'w')
    for line in reports:
        f.write(line)
    f.close()

    if result > 0:
        return render_template('matriks_c45.html', matriks_c45s=confusion_matrix)

    else:
        msg = 'No Matriks C4.5 Found'
        return render_template('matriks_c45.html', msg=msg)
    # Close connection
    cur.close()


# Data Hasil KNN
@app.route('/hasil_knn')
def hasil_knn():
    # Create cursor
    cur = mysql.connection.cursor()
    # Get test
    result = cur.execute("SELECT npm, nm_mhs, angkatan, nm_matkul, nilai, semester, status FROM test UNION SELECT npm, nm_mhs, angkatan, nm_matkul, nilai, semester, status FROM train")
    hasil_query_select = cur.fetchall()
    data = pd.DataFrame(hasil_query_select, columns=['npm', 'nm_mhs', 'angkatan', 'nm_matkul', 'nilai', 'semester', 'status'])

    data_pivot = pd.pivot_table(data, values='nilai', index='npm', columns='nm_matkul')
    data_pivot = data_pivot.merge(data[['npm','status']].drop_duplicates(),left_on='npm',right_on='npm')
    data_pivot = data_pivot.fillna(-1) # mengganti nilai nan menjadi -1

    X = data_pivot.drop(['npm','status'],axis=1)
    y = data_pivot['status']
    y.loc[y=='TERLAMBAT'] = 'Terlambat'
    y.loc[y=='Tidak Lulus'] = 'Terlambat'
    y.loc[y=='LULUS'] = 'Lulus'
    
    X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state=42)

    clf = DecisionTreeClassifier()
    clf.fit(X_train, y_train)
    print(classification_report(y_test,clf.predict(X_test)))

    data = data.replace('',np.nan, regex=True)
    train = data[pd.notnull(data.semester)][['nm_matkul','semester']]
    test = data[pd.isnull(data.semester)][['nm_matkul','semester']]

    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.preprocessing import LabelEncoder
    rec = KNeighborsClassifier()
    le = LabelEncoder()

    le.fit(train['nm_matkul'])
    X_train = le.transform(train['nm_matkul'])
    X_test = le.transform(test['nm_matkul'])
    y_train = train['semester']
    y_test = test['semester']

    X_train = X_train.reshape(len(X_train),1)
    X_test = X_test.reshape(len(X_test),1)
    rec.fit(X_train,y_train)
    
    test['semester'] = rec.predict(X_test)
    
    rec.fit(X_train, y_train)
    print(classification_report(y_test,rec.predict(X_test)))
    reports = metrics.classification_report(y_test,rec.predict(X_test))
    f = open('static/classification_report_knn.txt', 'w')
    for line in reports:
        f.write(line)
    f.close()

    if result > 0:
        return render_template('hasil_knn.html',  hasil_knn=[test.to_html(classes='data')], titles=test.columns.values)

    else:
        msg = 'No Hasil KNN Found'
        return render_template('hasil_knn.html', msg=msg)
    # Close connection
    cur.close()

if __name__ == '__main__':
    app.secret_key='secret123'
    app.run(debug=True)
